let questionCourante1 = 0
let questionCourante2 = 1
let questionCourante3 = 2
$('#quiz').hide()

$(function () {
    evenementDeLaPage()
    creerQuestion1(questions[questionCourante1])
    creerQuestion2(questions[questionCourante2])
    creerQuestion3(questions[questionCourante3])
     
    //creerTable(donnees)
})
function evenementDeLaPage(){

    $('#soumettre').on('click',function(){
        ValidationInformation() 
        AfficherInformation()
        calculerAge()
    })


    $('#valider').on('click',function(){
            let score = 0
            let rep1, rep2, rep3 = "faux"

            let reponseRecu1 = $(`input[name='Q1']:checked`).val();
            let reponseRecu2 = $(`input[name='Q2']:checked`).val();
            let reponseRecu3 = $(`input[name='Q3']:checked`).val();

            
            if(reponseRecu1 == questions[0].reponse){
                alert(reponseRecu1 + '   ' + questions[0].reponse + 'bonne réponse 1')
                score = score + 1
                rep1 = "vrai"
                alert('votre score de bonne réponse est ' + score + rep1)
            }else{
                alert('mauvaise rép 1')
            }

            if(reponseRecu2 == questions[1].reponse){
                alert(reponseRecu2 + '   ' + questions[1].reponse + 'bonne réponse 2')
                score = score + 1
                rep2 = "vrai"
                alert('votre score de bonne réponse est ' + score + rep2)
            }else{
                alert('mauvaise rép 2')
            }
            
            if(reponseRecu3 == questions[2].reponse){
                alert(reponseRecu3 + '   ' +  questions[2].reponse + 'bonne réponse 3')
                score = score + 1
                rep3 = "vrai"
                alert('votre score de bonne réponse est ' + score + rep3)
            }else{
                alert('mauvaise rép 3')
            }

            if (rep1 === "vrai" && rep3 === "vrai" && rep3 === "vrai"){
                alert('BRAVO')
            }
            // creerQuestion1(question)
            // creerQuestion2(question)
            // creerQuestion3(question)
            // const donneesEntrantes = `
            // [{
            //     "id":"1",
            //     "question":"${question1.question}",
            //     "reponses":["${question1.reponses[0]}", "${question1.reponses[1]}", "${question1.reponses[3]}"],
            //     "reponse":0
            //     "bonne" : "rep1", 
            //     "reponseFournie":"reponseRecu1",
            // },{
            //     "id":"2",
            //     "question":"${question2.question}",
            //     "reponses":["${question2.reponses[0]}", "${question2.reponses[1]}", "${question2.reponses[3]}"],
            //     "reponse":1
            //     "bonne" : "rep1", 
            //     "reponseFournie":"reponseRecu2",
            // },{
            //     "id":"3",
            //     "question":"${question3.question}",
            //     "reponses":["${question3.reponses[0]}", "${question3.reponses[1]}", "${question3.reponses[3]}"],
            //     "reponse":2
            //     "bonne" : "rep3", 
            //     "reponseFournie":"reponseRecu3",
            // }]
            // `
    })

 }




// Données
let questions = `
[
	{
		"question":"Comment se nomme un tiret empêchant la coupure de 2 mots ? Exemple : Jean-Pierre?",
		"reponses":[
		"tiret insécable",
		"trait d'union", 
		"Je ne sais pas"
		], 
		"reponse":0
	},
	{
		"question":"Comment se nomme le coloriage d'un fond de paragraphe ?",
		"reponses":[
			"une couleur de fond",
			"une trame de fond", 
			"Je ne sais pas"
		], 
		"reponse":1
	},
    {
		"question":"A quelle entité de texte s'applique la pose de taquets de tabulation sur la règle ?",
		"reponses":[
			"Aux paragraphes sélectionnés",
			"A la ligne sur laquelle se trouve le curseur", 
			"Je ne sais pas"
		], 
		"reponse":2
	}
]
`
questions = JSON.parse(questions)


//numeroQuestion = -1

$.validator.addMethod( "alphanumeric", function( value, element ) {
    return this.optional( element ) || /^\w+$/i.test( value );
    }, "Letters, numbers, and underscores only please" );

function ValidationInformation() {
    $("form[name='enregistrement']").validate(
    {
    rules: {
    prenom: {
                required :true,
                alphanumeric: true
            },
    nom: {
                required: true,
                alphanumeric: true
            },
            dateNaissance: {
        required: true
    },
    statut: {
        required: true
    }
    },
    messages: {
        nom: {
            required :'Nom obligatoire'
            },
        prenom: {
            required :'Prénom obligatoire'
            },
            dateNaissance: {
            required :'Date obligatoire'
            },
        statut: {
            required :'Statut obligatoire'
            }
    },
    submitHandler: function (form) {
        $('#enregistrement').hide()
    },
    showErrors: function (errorMap, errorList) {
        if (est_soumis) {
        var sommaire = "Vous avez les erreurs: \n";
        const ul = $('<ul></ul>')
        $.each(errorList, function () { ul.append(`<li>${this.message}</li>`) })
        $('#sommaire-erreurs').html(ul)
        est_soumis = false;
        }
        this.defaultShowErrors();
    },
    invalidHandler: function (form, validator) {
        est_soumis = true;
    }
    });
}

// créer Question

function creerQuestion1(question){
    let q1 = $(`<p>${question.question}</p>`)
    $('.question1').append(q1)
    let q1r1 = $(`<p>${question.reponses[0]}</p>`)
    $('.Q1R1').append(q1r1)
    let q1r2 = $(`<p>${question.reponses[1]}</p>`)
    $('.Q1R2').append(q1r2)
    let q1r3 = $(`<p>${question.reponses[2]}</p>`)
    $('.Q1R3').append(q1r3)
}

function creerQuestion2(question){
    let q2 = $(`<p>${question.question}</p>`)
    $('.question2').append(q2)
    let q2r1 = $(`<p>${question.reponses[0]}</p>`)
    $('.Q2R1').append(q2r1)
    let q2r2 = $(`<p>${question.reponses[1]}</p>`)
    $('.Q2R2').append(q2r2)
    let q2r3 = $(`<p>${question.reponses[2]}</p>`)
    $('.Q2R3').append(q2r3)
}

function creerQuestion3(question){
    let q3 = $(`<p>${question.question}</p>`)
    $('.question3').append(q3)
    let q3r1 = $(`<p>${question.reponses[0]}</p>`)
    $('.Q3R1').append(q3r1)
    let q3r2 = $(`<p>${question.reponses[1]}</p>`)
    $('.Q3R2').append(q3r2)
    let q3r3 = $(`<p>${question.reponses[2]}</p>`)
    $('.Q3R3').append(q3r3)
}

// Afficher les infotmations
function AfficherInformation(){
    let affnom = $('#nom').val()
    $(".affnom").html(affnom)
    let affprenom = $('#prenom').val()
    $(".affprenom").html(affprenom)
    let affstatut = $("select[name='statut'] > option:selected")
    $(".affstatut").html(affstatut)
}


// Afiicher le Formulaire
function AfficherFormulaire(){
    $('#formulaire').show()
    $('#quiz').hide()
}
// Cacher le Formulaire
function CacherFormulaire(){
    $('#formulaire').fadeOut()
    $('#quiz').fadeIn(3000)
}
        // afficherLeQuiz()

 //Calculer l'age
function calculerAge() {
    let fullDate = new Date();
    let annee = fullDate.getFullYear();
    let naissance = parseInt($('#ddn').val());
    let age = annee - naissance;
    if(age > 18 ){
        CacherFormulaire()
        const h4 = $('<h4>Majeur, vous avez : ' + age +' ans</h4>')
        h4.addClass('')
        $('.InformationsParticipant').html(h4)
    }else{
        alert('ATTENTION VOTRE AGE EST MINEUR')
        AfficherFormulaire()
    }
}
